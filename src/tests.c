#define _DEFAULT_SOURCE

#include "mem.h"
#include "mem_internals.h"
#include "tests.h"


const int64_t HEAP_SIZE = 7777;

static void destroy_heap(void* heap, size_t size){
    munmap(heap, size_from_capacity((block_capacity){size}).bytes);
}


static bool test1() {
    struct block_header* heap = heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);
    if (heap == NULL) {
        return false;
    }
    size_t size = 1234;
    void* mal = _malloc(size);
    if (mal == NULL) {
        return false;
    }
    debug_heap(stdout, heap);
    bool res = heap->capacity.bytes == size;
    _free(mal);
    debug_heap(stdout, heap);
    destroy_heap(heap, REGION_MIN_SIZE);
    return res;
}

static bool test2() {
    struct block_header* heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);
    if (heap == NULL) {
        return false;
    }
    void *mal1 = _malloc(1234);
    void *mal2 = _malloc(1234);
    void *mal3 = _malloc(1234);
    if (mal1 == NULL || mal2 == NULL || mal3 == NULL) {
        return false;
    }
    debug_heap(stdout, heap);
    _free(mal1);
    debug_heap(stdout, heap);
    _free(mal2);
    _free(mal3);
    destroy_heap(heap, HEAP_SIZE);
    return true;
}

static bool test3() {
    struct block_header* heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);
    if (heap == NULL) {
        return false;
    }
    void *mal1 = _malloc(1234);
    void *mal2 = _malloc(1234);
    void *mal3 = _malloc(1234);
    if (mal1 == NULL || mal2 == NULL || mal3 == NULL) {
        return false;
    }
    debug_heap(stdout, heap);
    _free(mal1);
    _free(mal2);
    debug_heap(stdout, heap);
    _free(mal3);
    destroy_heap(heap, HEAP_SIZE);
    return true;
}

static bool test4() {
    struct block_header* heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);
    if (heap == NULL) {
        return false;
    }
    void *mal = _malloc(HEAP_SIZE*10);
    if (mal == NULL) {
        return false;
    }
    debug_heap(stdout, heap);
    _free(mal);
    debug_heap(stdout, heap);
    destroy_heap(heap, HEAP_SIZE);
    return true;
}

static bool test5() {
    struct block_header* heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);
    if (heap == NULL) {
        return false;
    }
    void* mal1 = _malloc(HEAP_SIZE);
    if (mal1 == NULL) {
        return false;
    }
    debug_heap(stdout, heap);
    struct block_header* next = mal1 - offsetof(struct block_header, contents);
    void* new_addr = next->contents + next->capacity.bytes;
    void* temp = mmap(new_addr,HEAP_SIZE,PROT_READ | PROT_WRITE,
                      MAP_PRIVATE | MAP_ANONYMOUS,-1,0);
    void* mal2 = _malloc(5000);
    if (mal2 == NULL) {
        return false;
    }
    debug_heap(stdout, heap);
    _free(mal1);
    _free(mal2);
    destroy_heap(temp, HEAP_SIZE);
    destroy_heap(heap, HEAP_SIZE);
    return true;
}

bool test_all() {
    bool res = true;
    for (size_t i = 1; i <= 5; i++) {
        bool iter_res = false;
        switch (i) {
            case 1:
                iter_res = test1(); break;
            case 2:
                iter_res = test2(); break;
            case 3:
                iter_res = test3(); break;
            case 4:
                iter_res = test4(); break;
            case 5:
                iter_res = test5(); break;
        }
        if (iter_res) {
            printf("\nTest %zu passed\n", i);
        } else {
            printf("\nTest %zu failed\n", i);
            res = false;
        }
    }
    return res;
}
