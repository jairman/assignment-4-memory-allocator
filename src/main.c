#define _DEFAULT_SOURCE
#include <stdio.h>

#include "tests.h"


int main() {
    if (test_all()) {
        printf("All tests passed!\n");
        return 0;
    }
    printf("Tests failed\n");

    return 1;
}
